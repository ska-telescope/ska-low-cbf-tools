# -*- coding: utf-8 -*-
#
# This file is part of the CommandLineDevice project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Launch a command with configurable arguments
"""
# Tango conventions clash with Python conventions...
# pylint: disable=invalid-name, attribute-defined-outside-init

import time

from tango import AttrQuality, AttrWriteType
from tango.server import Device, attribute, command, device_property, run

from tango_cmdline.configurable_command import ConfigurableCommand

__all__ = ["CommandLineDevice", "main"]


class CommandLineDevice(Device):
    """
    Launch a command with configurable arguments

    **Properties:**
    - Device Property
      Template
        - String template, with user configurable parameters indicated by a
        leading `$`.  e.g. ``ls -la $path``
        - Type:'DevString'
    """

    # Device Properties
    Template = device_property(dtype="DevString", mandatory=True)

    # Attributes
    pids = attribute(
        dtype=("DevULong64",),
        max_dim_x=255,
        label="Active PIDs",
        doc="PIDs of running processes",
    )

    args = attribute(
        dtype=("DevString",),
        max_dim_x=255,
        label="Arguments",
        doc=(
            "List of available arguments (these will be made available as "
            "dynamic attributes)"
        ),
    )

    stdout = attribute(
        dtype="DevString",
        label="Standard Output",
        doc="Output from last execution",
    )
    stderr = attribute(
        dtype="DevString",
        label="Standard Error",
        doc="Error output from last execution",
    )

    # General methods
    def init_device(self):
        """
        Initialises the attributes and properties of the CommandLineDevice.
        """
        Device.init_device(self)
        self._command = ConfigurableCommand(self.Template)
        for arg in self._command.args:
            attr = attribute(
                name=arg,
                label=arg,
                dtype=str,
                access=AttrWriteType.READ_WRITE,
                format="%s",
            )
            self.add_attribute(
                attr, r_meth=self._read_arg, w_meth=self._write_arg
            )

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """

    # Attributes methods
    def read_pids(self):
        """Return the pids attribute."""
        return self._command.pids

    def read_args(self):
        """Return the args attribute."""
        return self._command.args

    def read_stdout(self):
        """Return the stdout attribute."""
        return self._command.stdout

    def read_stderr(self):
        """Return the stderr attribute."""
        return self._command.stderr

    def _read_arg(self, attr):
        """Read an argument attribute"""
        try:
            name = attr.get_name()
        except AttributeError as error:
            print("attribute error {}".format(error))
            return

        attr.set_value_date_quality(
            self._command[name], time.time(), AttrQuality.ATTR_VALID
        )

    def _write_arg(self, attr):
        """Write an argument attribute"""
        try:
            name = attr.get_name()
        except AttributeError as error:
            print("attribute error {}".format(error))
            return

        value = attr.get_write_value()
        self._command[name] = value

    # Commands
    @command()
    def Execute(self):
        """
        Execute the command. Arguments will be taken from dynamic attributes.

        :return:None
        """
        self._command.execute()


# Run server
def main(args=None, **kwargs):
    """Main function of the CommandLineDevice module."""
    return run((CommandLineDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
