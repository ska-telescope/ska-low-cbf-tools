# This program reads a pcap packet file captured from the correlator
# filterbank output and compares it with output from the 'packetised model'
# to allow checking that the output from the filterbank FPGA implementation
# matches the model.
#
# Copyright CSIRO 2020
import sys
from scapy.utils import RawPcapReader
from struct import unpack
import numpy as np
import matplotlib.pyplot as plt

BYTES_IN_PKT = 4680

NUM_STATIONS = 6
NUM_CHANS = 384
NUM_FINE_CHANS = 3456
NUM_FINE_CHAN_PER_PKT = 576
NUM_TIME_SAMPLES = 408

def read_fine_filt_pcap(pcap_file):
  # read file once to get number of packets, stations, coarse chans, samples
  r = RawPcapReader(pcap_file)
  kk = r.read_packet()
  if not kk:
    print('no packets in file')
    return None

  vchans = []
  stn_id0s = []
  stn_id1s = []
  stn_all_ids = []
  fine_chan_starts = []
  seq_nos = []
  pkt_count = 0;
  wrong_size_count = 0
  while kk:
    pkt_count += 1
    pkt,z,(sec,usec,siz) = kk
    if siz != BYTES_IN_PKT:
        wrong_size_count += 1
        kk = r.read_packet()
        continue
    fpga_hdr = pkt[42:56]
    (s) = unpack('>xxxxHxxxxxxxx',fpga_hdr)
    seq_nos.append(s[0])
    pkt_hdr = pkt[56:72]
    (tstamp, vchan, stn_id0, stn_id1, fchan, dly0, dly1) = unpack(
            '<IHHHHHH', pkt_hdr)
    if vchan not in vchans:
        vchans.append(vchan)
    if stn_id0 not in stn_id0s:
        stn_id0s.append(stn_id0)
    if stn_id1 not in stn_id1s:
        stn_id1s.append(stn_id1)
    if fchan not in fine_chan_starts:
        fine_chan_starts.append(fchan)
    if stn_id0 not in stn_all_ids:
        stn_all_ids.append(stn_id0)
    if stn_id1 not in stn_all_ids:
        stn_all_ids.append(stn_id1)

    #read next packet
    kk = r.read_packet()
  del r
  # now that it's read, summarise what's in pcap file
  print('{} packets'.format(pkt_count))
  if wrong_size_count != 0:
    print('{} packets not correct size ({} bytes)'.format(wrong_size_count,
            BYTES_IN_PKT))
  print('{} distinct virtual channels (coarse channels)'.format(len(vchans)))
  print('{} distinct ids for stn1'.format(len(stn_id0s)))
  print('{} distinct ids for stn2'.format(len(stn_id1s)))
  print('{} packets per filter time sample'.format(len(fine_chan_starts)))

  # plot the sequence numbers from each packet
#  delta = []
#  for seqnoz in range(0,len(seq_nos)-1):
#     diff = seq_nos[seqnoz+1] - seq_nos[seqnoz]
#     delta.append(diff)

  fig,ax = plt.subplots()
#  ax.scatter(range(0,len(delta)), delta)
  ax.scatter(range(0,len(seq_nos)), seq_nos)
  ax.set_xlabel('pkt_no')
  ax.set_ylabel('seq_no')
  plt.grid()
  plt.show()

  #read file a second time to read the data

  num_vchans = len(vchans)
  num_fine_chan = len(fine_chan_starts) * NUM_FINE_CHAN_PER_PKT
  num_stns = len(stn_all_ids)
  num_samples = 408

  vchan_to_idx = {}
  for idx,vchnl in enumerate(vchans):
      vchan_to_idx[vchnl] = idx
  stn_to_idx = {}
  for idx,stn in enumerate(stn_all_ids):
      stn_to_idx[stn] = idx
  fch_to_idx = {}
  for idx,fch in enumerate(fine_chan_starts):
      fch_to_idx[fch] = idx

  filt_data_re = np.zeros((num_stns, num_vchans, num_fine_chan, 2, num_samples),
          dtype=np.int8)
  filt_data_im = np.zeros((num_stns, num_vchans, num_fine_chan, 2, num_samples),
          dtype=np.int8)

  tstamps = np.zeros((num_stns, num_vchans, len(fine_chan_starts)), dtype=np.int32)
  
  nzz = 0
  sampl = 0
  pkt_num = 0
  f = open('z.txt','w')
  last_stn0 = -1
  last_stn1 = -1
  last_vchan = -1
  grp_cnt = 0
  r = RawPcapReader(pcap_file)
  kk = r.read_packet()
  while kk:
    # show progress reading packets
    grp_cnt += 1
    nzz += 1
    if nzz % 50000 == 0:
        print('{} packets {:.1f}%'.format(nzz, nzz*100.0/pkt_count))
    # decode pcap file information into its pieces
    pkt,z,(sec,usec,siz) = kk
    if siz != BYTES_IN_PKT:
        wrong_size_count += 1
        kk = r.read_packet()
        continue
    # decode headers
    fpga_hdr = pkt[42:56]
    (s) = unpack('>xxxxHxxxxxxxx',fpga_hdr)
    seq_nos.append(s[0])
    pkt_hdr = pkt[56:72]
    (tstamp, vchan, stn_id0, stn_id1, fchan, dly0, dly1) = unpack(
            '<IHHHHHH', pkt_hdr)
    #
    # decode filter data encoded in packet
    stn_idx0 = stn_to_idx[stn_id0]
    stn_idx1 = stn_to_idx[stn_id1]
    vchan_idx = vchan_to_idx[vchan]
    fchan_idx = fch_to_idx[fchan]
    if ((last_stn0 != stn_id0) or (last_stn1 != stn_id1) or(last_vchan != vchan)) and (fchan == 0):
        last_stn0 = stn_id0
        last_stn1 = stn_id1
        last_vchan = vchan
        f.write(' {} packets stn=[{},{}], vchan={}\n'.format(grp_cnt, stn_id0,stn_id1, vchan))
        grp_cnt = 0
    ftxt = 'stn= [{},{}]   vChan={:4d}   fineChan_start= {:4d}\n'.format( 
            stn_id0, stn_id1, vchan, fchan)
    f.write(ftxt)
    pkt_data = pkt[72:]
    pkt2 = np.frombuffer(pkt_data, np.int8)
    ts  = tstamps[stn_idx0, vchan_idx, fchan_idx]
    ts2 = tstamps[stn_idx1, vchan_idx, fchan_idx]
    filt_data_re[stn_idx0, vchan_idx, fchan:(fchan+576),0,ts] = pkt2[0:(576*8):8]
    filt_data_im[stn_idx0, vchan_idx, fchan:(fchan+576),0,ts] = pkt2[1:(576*8):8]
    filt_data_re[stn_idx0, vchan_idx, fchan:(fchan+576),1,ts] = pkt2[2:(576*8):8]
    filt_data_im[stn_idx0, vchan_idx, fchan:(fchan+576),1,ts] = pkt2[3:(576*8):8]
    filt_data_re[stn_idx1, vchan_idx, fchan:(fchan+576),0,ts2] = pkt2[4:(576*8):8]
    filt_data_im[stn_idx1, vchan_idx, fchan:(fchan+576),0,ts2] = pkt2[5:(576*8):8]
    filt_data_re[stn_idx1, vchan_idx, fchan:(fchan+576),1,ts2] = pkt2[6:(576*8):8]
    filt_data_im[stn_idx1, vchan_idx, fchan:(fchan+576),1,ts2] = pkt2[7:(576*8):8]
    tstamps[stn_idx0, vchan_idx, fchan_idx] += 1
    tstamps[stn_idx1, vchan_idx, fchan_idx] += 1
    
    #
    #read next packet
    kk = r.read_packet()
  f.close()
  # write to file number of packets found for each vchan, stn
  with open('counts.txt','w') as f2:
    f2.write('      Fine Chan Grp:  A  B  C  D  E  F\n')
    for vvch in range(0,128):
      vi = vchan_to_idx[vvch]
      for stn in range(0,6):
        si = stn_to_idx[stn]
        txt2 = 'vchan={} stn={} count={}\n'.format(vvch, stn, tstamps[si, vi, :])
        f2.write(txt2)
  print('highest times written in file: counts.txt')

  return (filt_data_re, filt_data_im, stn_to_idx, vchan_to_idx)

def get_model( data_f, ptr_f, pstn, pch, ppol, ptim, pfin):
  checkData_filename = data_f
  checkPtrs_filename = ptr_f
  
  
  plotStation = pstn  # Station to plot, 0 to 5
  plotChannel = pch  # channel to plot, 0 to 127
  plotPol = ppol      # polarisation to plot, 0 vertical polarisation, 1 horizontal polarisation
  plotTime = ptim     # first plot : plot this time (0 to 203) for all 3456 fine channels.
  plotFine = pfin  # second plot : Plot this fine channel (0 to 3455) for all 204 times. 
  
  # Get the raw data.
  checkData = np.fromfile(checkData_filename, np.int8)
  checkPtrs = np.loadtxt(checkPtrs_filename, dtype = np.int64, delimiter = ',')

  # select 204 times for a particular channel, station, fine channel, and polarisation.
  p1 = checkPtrs[plotChannel,plotStation] - 1
  if (p1 == -1):
    time_re = np.zeros(204,dtype = np.int8)
    time_im = np.zeros(204,dtype = np.int8)
    fine_re = np.zeros(3456,dtype = np.int8)
    fine_im = np.zeros(3456,dtype = np.int8)
  else:
    time_re = checkData[(p1+plotFine*4+plotPol*2):(p1+(4*3456*204)):(4*3456)]
    time_im = checkData[(p1+plotFine*4+plotPol*2+1):(p1+(4*3456*204)):(4*3456)]
    # select all 3456 fine channels for a particular channel, station, polarisation and time.
    fine_re = checkData[(p1 + plotTime*4*3456 + plotPol*2):(p1 + plotTime*4*3456 + (4*3456)):4]
    fine_im = checkData[(p1 + plotTime*4*3456 + plotPol*2+1):(p1 + plotTime*4*3456 + (4*3456)):4]
  
  return (time_re.astype(np.float), time_im.astype(np.float),
          fine_re.astype(np.float), fine_im.astype(np.float))


if __name__ == '__main__':
  if len(sys.argv) < 2:
    print('Read Fine Filter packets captured from 25GbE debug port')
    print('Usage:\n {}  pcap_packet_file'.format(sys.argv[0]))
    sys.exit()

  pcap_filename = sys.argv[1]
  (ff_re, ff_im, stn2idx, vchan2idx) = read_fine_filt_pcap(pcap_filename)
  # first two array dimensions are [stn, vchan, fine_chan, pol, time]
  # second two are dictionaries
  plot_station = 1
  plot_vchan = 1
  plot_fine = 1825
  plot_pol = 0
  plot_sampl = 1
#  plot_sampl = 0

  stn_idx = stn2idx[plot_station]
  vchan_idx = vchan2idx[plot_vchan]
  print('Plotting station {} (index {}) and vchan {} (index{})'.format(
              plot_station, stn_idx, plot_vchan, vchan_idx))
  

# all time for a station, vchan, fine_chan, pol
  plt_re = ff_re[stn_idx, vchan_idx, plot_fine, plot_pol, :].astype(np.float)
  plt_im = ff_im[stn_idx, vchan_idx, plot_fine, plot_pol, :].astype(np.float)
# all fine freqs for a station, vchan, pol, sample_no
  fd_re = ff_re[stn_idx, vchan_idx, :, plot_pol, plot_sampl].astype(np.float)
  fd_im = ff_im[stn_idx, vchan_idx, :, plot_pol, plot_sampl].astype(np.float)
# convert re/im to magnitude
  fd_mag = np.sqrt(fd_re*fd_re + fd_im*fd_im)
  td_mag = np.sqrt(plt_re*plt_re + plt_im*plt_im)

  txt = 'Station numbers:'
  for k in stn2idx.keys():
    txt = txt + ' {}'.format(k)
  print(txt)

  rslt = get_model(
          'run5/FDCaptureData_FPGA1.raw',
          'run5/FDCapturePtrs_fpga1.txt',
          plot_station, plot_vchan, plot_pol, plot_sampl, plot_fine)
#          plot_station-1, plot_vchan, plot_pol, plot_sampl+1, plot_fine)
  if rslt:
    (ref_plt_re, ref_plt_im, ref_fd_re, ref_fd_im) = rslt
    ref_fd_mag = np.sqrt(ref_fd_re*ref_fd_re + ref_fd_im*ref_fd_im)
    ref_td_mag = np.sqrt(ref_plt_re*ref_plt_re + ref_plt_im*ref_plt_im)
  
  fig,ax = plt.subplots()
  ax.plot(range(0,len(plt_re)), plt_re,'r.-')
  ax.plot(range(0,len(plt_re)), plt_im,'g.-')
  ax.plot(range(0,len(ref_plt_re)), ref_plt_re, 'b.-')
  ax.plot(range(0,len(ref_plt_im)), ref_plt_im, 'c.-')
  ax.set_xlabel('sample')
  ax.set_ylabel('re')
  ax.set_title('Time-domain samples for stn{}, vchan{}, finechan{}, pol{} (red=captured, grn=model)'.format(
              plot_station, plot_vchan, plot_fine, plot_pol))
  plt.grid()

  fig2,ax2 = plt.subplots()
  #ax2.scatter(range(0,len(fd_mag)), fd_mag)
  ax2.plot(range(0,len(fd_mag)), fd_mag, 'r+')
  ax2.plot(range(0,len(ref_fd_mag)), ref_fd_mag, 'g+')
  ax2.set_xlabel('Fine Channel')
  ax2.set_ylabel('Magnitude')
  ax2.set_title('Fine chans for stn{}, vchan{}, pol{}, sample{} (red=captured, green=model)'.format(
              plot_station, plot_vchan, plot_pol, plot_sampl))
  plt.grid()
  
  fig3,ax3 = plt.subplots()
  #ax2.scatter(range(0,len(fd_mag)), fd_mag)
  ax3.plot(range(0,len(td_mag)), td_mag, 'r+')
  ax3.plot( ref_td_mag[0:204], 'g+')
  ax3.set_xlabel('Sample number')
  ax3.set_ylabel('Magnitude')
  ax3.set_title('Time domain samples for stn{}, vchan{}, pol{} (red=captured, grn=model)'.format(
              plot_station, plot_vchan, plot_pol))
  plt.grid()

  plt.show()

  exit(0)
   
