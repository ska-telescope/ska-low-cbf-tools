#include "hbm_dump_sharedmem.h"
#include <iostream> // for cin cout cerr
#include <cstring> // for memcpy
//#include "utils.h"

HBMDumpSharedMem * HBMDumpSharedMem::create(bool isOwner, int mem_idx)
{
    std::string mem_name("/hbm-dump-data");
    mem_name.append( std::to_string(mem_idx));
    HBMDumpSharedMem * h = new HBMDumpSharedMem(isOwner, mem_name);
    if( ! h->isOk() )
    {
        delete h;
        return nullptr;
    }
    return h;
}

HBMDumpSharedMem::HBMDumpSharedMem(bool isOwner, std::string mem_name)
    : PosixSharedMem(mem_name, sizeof(Shared_info), isOwner)
    , m_ok(false)
    , m_isOwner(isOwner)
{
    m_mem = (Shared_info *) m_addr;
    if(m_addr == nullptr)
    {
        std::cout << "Couldnt create Posix shared memory" << std::endl;
        return;
    }
    if(m_isOwner)
    {
        int ok1 = sem_init(&m_mem->updated, 1, 0); // shared, not ready for read
        if(ok1 != 0)
            std::cerr << "sem init error on updated sem\n";
        m_mem->hbm_idx = 0;
        int ok2 = sem_init(&m_mem->requested, 1, 0); // shared, no dump rquested
        if(ok2 != 0)
            std::cerr << "sem init error on requested sem\n";
        int ok3 = sem_init(&m_mem->access, 1, 1); // shared, data accessible
        if(ok3 != 0)
            std::cerr << "sem init error on access sem\n";
        m_ok = (ok1 == 0) && (ok2 == 0) && (ok3 == 0);
    }
    else
    {
        m_ok = true;
    }

}

HBMDumpSharedMem::~HBMDumpSharedMem()
{
    if(m_isOwner && (m_addr != nullptr))
    {
        sem_destroy(&m_mem->updated);
        sem_destroy(&m_mem->requested);
        sem_destroy(&m_mem->access);
    }
}

void HBMDumpSharedMem::dump_request(uint32_t hbm_idx)
{
    int rv;
    do // acquire semaphore to get access to shared mem data
    {
        rv = sem_wait(&m_mem->access);
    } while(rv == EINTR);
    m_mem->hbm_idx = hbm_idx;
    sem_post(&m_mem->access); // relinquish access

    sem_post(&m_mem->requested);
    return;
}

int HBMDumpSharedMem::check_for_request()
{
    int rv = sem_trywait(&m_mem->requested);
    if(rv == -1) // wait failed due no request TODO: better checks
        return -1;

    //do // acquire semaphore to get access to shared mem data
    //{
    //    rv = sem_wait(&m_mem->access);
    //} while(rv == EINTR);
    int hbm_idx = m_mem->hbm_idx;
    sem_post(&m_mem->access); // relinquish access

    // clear out any old requests received before now
    int value;
    sem_getvalue(&m_mem->requested, &value);
    while(value > 0)
    {
        sem_wait(&m_mem->requested);
        sem_getvalue(&m_mem->requested, &value);
    }

    std::cout << "Got request for HBM[" << hbm_idx << "]" << std::endl;
    return hbm_idx;
}

void HBMDumpSharedMem::wait_write_to_share(void *buffer, size_t len_bytes)
{
    if(!m_ok)
        return;

    int rv;
    do // wait to get access to shared mem data
    {
        rv = sem_wait(&m_mem->access);
    } while(rv == EINTR);
    if(rv != 0)
    {
        std::cerr << "Sem_wait error\n";
        return;
    }
    memcpy(m_mem->hbm_data, buffer, len_bytes);
    sem_post(&m_mem->access); // relinquish access

    sem_post(&m_mem->updated); // signal that new data is ready to read

    return;
}

void HBMDumpSharedMem::wait_read_from_share(void *buf, size_t len)
{
    if(!m_ok)
    {
        return;
    }

    int rv;
    do 
    {
        rv = sem_wait(&m_mem->updated);
    } while(rv == EINTR);

    if(rv != 0)
    {
        std::cerr << "Sem_wait error\n";
        return;
    }

    do 
    {
        rv = sem_wait(&m_mem->access);
    } while(rv == EINTR);

    if(rv != 0)
    {
        std::cerr << "Sem_wait error\n";
        return;
    }


    int value;
    sem_getvalue(&m_mem->updated, &value);
    while(value > 0)
    {
        sem_wait(&m_mem->updated);
        sem_getvalue(&m_mem->updated, &value);
    }
    
    memcpy(buf, m_mem->hbm_data, len);

    sem_post(&m_mem->access);
    return;
}

bool HBMDumpSharedMem::isOk()
{
    return m_ok;
}
