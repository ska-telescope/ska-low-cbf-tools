/* A class to interpret gemini-protocol packets received by a Gemini server
 * performing the register-read or register-write operations requested
 */
/* Copyright (c) 2020, CSIRO. */

#ifndef GEMINI_H
#define GEMINI_H

#include <functional>

class Gemini
{
    private:
        std::function<void(uint32_t, int, char*)> m_writer;
        std::function<void(uint32_t, int, char*)> m_reader;

        char m_last_seq;
    public:
        Gemini( std::function<void(uint32_t, int, char*)> m_writer
                ,std::function<void(uint32_t, int, char*)> m_reader);
        ~Gemini();
        Gemini(const Gemini &) = delete;
        Gemini& operator=(Gemini& rhs) = delete;

        void pkt_in( char* buf, size_t rx_len, size_t * tx_len );
};

#endif
