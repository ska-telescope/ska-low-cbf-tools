/* A class encapsulating the Xilinx OpenCL interface for Alveo cards.
 * Based on example code provided by Xilinx.
 */

/**********
Copyright (c) 2018, Xilinx, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********/

/* Copyright (c) 2020, CSIRO */

#include "alveo_cl.h"
#include "hbm_dump_sharedmem.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <CL/cl_ext_xilinx.h>
#include <unistd.h>
#include <limits.h>
#include <sys/stat.h>
#include <iomanip>  // for setw()
#include <chrono>

#define BUFFER_WORDS 1024
#define ONE_G_BYTE (1<<30)

#define OCL_CHECK(error,call)                                       \
    call;                                                           \
    if (error != CL_SUCCESS) {                                      \
      printf("%s:%d Error calling " #call ", error code is: %d\n",  \
              __FILE__,__LINE__, error);                            \
      exit(EXIT_FAILURE);                                           \
    }                                       

namespace xcl {
std::vector<cl::Device> get_devices(const std::string& vendor_name) {

    size_t i;
    cl_int err;
    std::vector<cl::Platform> platforms;
    OCL_CHECK(err, err = cl::Platform::get(&platforms));
    cl::Platform platform;
    for (i  = 0 ; i < platforms.size(); i++){
        platform = platforms[i];
        OCL_CHECK(err, std::string platformName = platform.getInfo<CL_PLATFORM_NAME>(&err));
        if (platformName == vendor_name){
            std::cout << "Found OpenCL Platform" << std::endl;
            std::cout << "Platform Name: " << platformName.c_str() << std::endl;
            break;
        }
    }
    if (i == platforms.size()) {
        std::cout << "Error: Failed to find Xilinx platform" << std::endl;
        exit(EXIT_FAILURE);
    }
   
    //Getting ACCELERATOR Devices and selecting 1st such device 
    std::vector<cl::Device> devices;
    OCL_CHECK(err, err = platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &devices));
    return devices;
}
   
std::vector<cl::Device> get_xil_devices() {
    return get_devices("Xilinx");
}

// Return buffer containing contents of xclbin file read from disk
char* read_binary_file(const std::string &xclbin_file_name, unsigned &nb) 
{
    std::cout << "Reading '" << xclbin_file_name
                << "' FPGA binary file" << std::endl;

    if(access(xclbin_file_name.c_str(), R_OK) != 0) {
        printf("ERROR: %s xclbin not available please build\n", xclbin_file_name.c_str());
        exit(EXIT_FAILURE);
    }
    //Loading XCL Bin into char buffer 
    std::ifstream bin_file(xclbin_file_name.c_str(), std::ifstream::binary);
    bin_file.seekg (0, bin_file.end);
    nb = bin_file.tellg();
    bin_file.seekg (0, bin_file.beg);
    char *buf = new char [nb];
    bin_file.read(buf, nb);
    std::cout << "'" << xclbin_file_name.c_str() << "' read" << std::endl;
    return buf;
}
};

// print a message and wait till user hits enter
void wait_for_user(const std::string &msg)
{
    std::cout << msg << std::endl;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return;
}

Alveo_cl::Alveo_cl(int device_idx)
    : m_device_idx(device_idx) // which device we want to use
    , m_sharedBuf(BUFFER_WORDS)
{
    bool is_owner = true;
    m_shmem = HBMDumpSharedMem::create(is_owner, device_idx);
    if(m_shmem != nullptr)
        std::cout << "Posix Shared Mem is OK" << std::endl;
}

Alveo_cl::~Alveo_cl()
{
    delete m_shmem;
    /* these class variables are deleted in order:
       m_kernel; m_program; m_q; m_context;
    */
}

// Parse HBM config string to determine how many HBM buffers, size, and
// whether they are shared with host or internal to FPGA
void Alveo_cl::hbm_configuration(std::string hbm_config)
{
    m_num_hbm = 0;

    std::istringstream strs(hbm_config);
    std::string token;
    char delimiter = ' ';
    while(std::getline(strs, token, delimiter))
    {
        int len = token.size();
        if(len <= 2)
            continue;
        if( (token.at(len-1) != 'i') && (token.at(len-1) != 's'))
            continue;
        if( (token.at(len-2) != 'M') && (token.at(len-2) != 'G'))
            continue;

        std::cout << "HBM: '" << token << "'" << std::endl;
        // All but last two chars are the size number
        uint64_t siz = std::stoul(token.substr(0,len-2), 0, 0);
        // Second last char says Gigabytes or Megabytes
        if( token.at(len-2) == 'M')
            siz = siz << 20; // 'M' is megabytes ie 2^20
        else
            siz = siz << 30; // 'G' is  gigabytes ie 2^30
        // Last char says if shared with host, or purely FPGA-internal
        bool shared = (token.at(len-1) == 's');

        m_hbm_size_bytes.push_back(siz);
        m_is_hbm_shared.push_back(shared);
        ++m_num_hbm;
    }
    std::cout << m_num_hbm << " HBM buffers defined" << std::endl;
}


int Alveo_cl::init(std::string xclbin_filename, std::string kernel_name
        , uint32_t args_base, std::string hbm_config, bool wait)
{
    std::cout << "Alveo OpenCL init..." << std::endl;

    m_shared_base_addr = args_base;
    std::string binaryFile = xclbin_filename;
    hbm_configuration(hbm_config);
    
    cl_int err;
    unsigned fileBufSize;

    // Get a list of all the Xilinx cards that are installed
    std::vector<cl::Device> devices = xcl::get_xil_devices();
    if(devices.size() == 0)
    {
        std::cout << "No Xilinx cards found" << std::endl;
        return EXIT_FAILURE;
    }

    // Show all Xilinx devices that were found
    std::cout << "=============\n" << "Xilinx cards:" << std::endl;
    for(unsigned int i=0; i<devices.size(); i++)
    {
        std::cout << i << ": ";
        std::string device_name = devices[i].getInfo<CL_DEVICE_NAME>(&err);
        if(err == CL_SUCCESS)
            std::cout << device_name << std::endl;
        else
            std::cout << "unknown" << std::endl;
    }
    std::cout << "=============" << std::endl;

    if((m_device_idx < 0) || ((unsigned)m_device_idx >= devices.size()))
    {
        std::cout << "Alveo device index " << devices.size()
            << " is outside range of installed devices [0.."
            << (devices.size() - 1) << "]" << std::endl;
        return EXIT_FAILURE;
    }
    // Create context & cmd queue for the chosen Alveo device
    cl::Device device = devices[m_device_idx];

    // Vector of devices to program with the bitfile
    devices.clear();
    devices.push_back(device);

    // Create OCL context and command queue
    m_context = cl::Context(device, NULL, NULL, NULL, &err);
    if(err != CL_SUCCESS)
    {
        std::cout << "Error creating OpenCL context" << std::endl;
        return EXIT_FAILURE;
    }
    m_q = cl::CommandQueue(m_context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    if(err != CL_SUCCESS)
    {
        std::cout << "Error creating OpenCL command queue" << std::endl;
        return EXIT_FAILURE;
    }

    // Ensure FPGA bitfile is loaded, copy to FPGA if not
    char* fileBuf = xcl::read_binary_file(binaryFile, fileBufSize);
    cl::Program::Binaries bins{{fileBuf, fileBufSize}};
    std::vector<int> fpga_load_ok;
    m_program = cl::Program(m_context, devices, bins
                        , &fpga_load_ok, &err);
    if(err != CL_SUCCESS)
    {
        std::cout << "Failed to create OpenCL program" << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "Created OpenCL program" << std::endl;
    int load_ok = 0;
    int load_fail = 0;
    for(unsigned int i=0; i<fpga_load_ok.size(); i++)
    {
        if(fpga_load_ok[i] == CL_SUCCESS)
            ++load_ok;
        else
            ++load_fail;
    }
    std::cout << load_ok << " of " << (load_ok + load_fail)
        << " FPGAs loaded with bitfile binary OK" << std::endl;

    m_kernel = cl::Kernel(m_program,"vitisAccelCore", &err);
    delete [] fileBuf;
    if(err != CL_SUCCESS)
    {
        std::cout << "Failed to create kernel" << std::endl;
        return EXIT_FAILURE;
    }

    // Does User need to attach ILA after FPGA loaded, before anything runs?
    if(wait)
    {
        wait_for_user("Enter when ready to proceed - Vivado ILA trigger set??\n");
    }

    // Allocate Global Memory Buffer for copying registers to/from FPGA card
    m_sharedBufGM = cl::Buffer(m_context,CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE
            , BUFFER_WORDS*sizeof(int), m_sharedBuf.data(), &err);
    if(err != CL_SUCCESS)
    {
        std::cout << "Failed to create buffer" << std::endl;
        return EXIT_FAILURE;
    }

    // Allocate additional Global Memory Buffer(s) used by FPGA
    for(int i=0; i<m_num_hbm; i++)
    {
        if(m_is_hbm_shared.at(i)) // HBM memory accessible on host side
        {
            std::cout << "Creating FPGA/host shared buffers ("
                << (m_hbm_size_bytes.at(i) >> 20) << " Mbyte) " << i << std::endl;

            // Alocate host-side memory buffer for PCIe DMA source/dest
            m_hostBufs.push_back( Host_buffer((size_t) m_hbm_size_bytes.at(i)
                        / sizeof(int) )); // TODO template magic not sizeof(int)

            // Allocate FPGA-side buffer linked to host-side buffer
            cl::Buffer fpga_buf = cl::Buffer(m_context
                    , CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE
                    ,(size_t) m_hbm_size_bytes.at(i)
                    , m_hostBufs.at(i).data(), &err);
            if(err != CL_SUCCESS)
            {
                std::cout << "Failed to create shared buffer ("
                    << (m_hbm_size_bytes.at(i) >> 20) << " Mbyte)" << std::endl;
                return EXIT_FAILURE;
            }
            m_fpgaBufs.push_back(fpga_buf);
        }
        else // HBM memory used on FPGA side but inaccessible from host
        {
            std::cout << "Creating FPGA-only buffer ("
                << (m_hbm_size_bytes.at(i) >> 20) << " Mbyte)" << std::endl;

            // This host-side buffer is unused, only a placelholder in the list
            m_hostBufs.push_back( Host_buffer() );

            // Allocate FPGA-side buffer, but not accessible from host side
            cl::Buffer fpga_buf = cl::Buffer(m_context
                    , CL_MEM_HOST_NO_ACCESS
                    ,(size_t) m_hbm_size_bytes.at(i)
                    , nullptr, &err);
            if(err != CL_SUCCESS)
            {
                std::cout << "Failed to create FPGA-only buffer ("
                    << (m_hbm_size_bytes.at(i) >> 20) << " Mbyte)" << std::endl;
                return EXIT_FAILURE;
            }
            m_fpgaBufs.push_back(fpga_buf);
        }

    }
    /*
    m_intl0Buf =  std::vector<int, aligned_allocator<int>>(ONE_G_BYTE);
    m_intl0BufGM = cl::Buffer(m_context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE
            , (size_t) ONE_G_BYTE, m_intl0Buf.data(), &err);
    if(err != CL_SUCCESS)
    {
        std::cout << "Failed to create first internal 1G buffer" << std::endl;
        return EXIT_FAILURE;
    }
    m_intl1BufGM = cl::Buffer(m_context, CL_MEM_HOST_NO_ACCESS, ONE_G_BYTE,
            nullptr, &err);
    if(err != CL_SUCCESS)
    {
        std::cout << "Failed to create second internal 1G buffer" << std::endl;
        return EXIT_FAILURE;
    }
*/


    std::cout << "Ready!" << std::endl;
    return EXIT_SUCCESS;
}

void Alveo_cl::read(uint32_t addr, uint32_t * buf, uint32_t len)
{
    // ARGS-space byte addresses of 32-bit register words to be transferred
    int argsSrcAddr = addr * 4;  // Address of src word
    int argsDestAddr = m_shared_base_addr * 4; // Addr of RAM shared VITIS
    int dmaLength = len * 4;  // Bytes requested (one 32-bit word = 4 bytes)

    cl_int err;
    // Set OpenCL kernel arguments sent to FPGA
    OCL_CHECK(err, err = m_kernel.setArg(0, argsSrcAddr));
    OCL_CHECK(err, err = m_kernel.setArg(1, argsDestAddr));
    OCL_CHECK(err, err = m_kernel.setArg(2, m_sharedBufGM));
    OCL_CHECK(err, err = m_kernel.setArg(3, dmaLength));
    for(int i=0; i<m_num_hbm; i++)
    {
        OCL_CHECK(err, err = m_kernel.setArg(4+i, m_fpgaBufs.at(i)));
    }
    //OCL_CHECK(err, err = m_kernel.setArg(4, m_intl0BufGM));
    //OCL_CHECK(err, err = m_kernel.setArg(5, m_intl1BufGM));
    // Run the kernel
    OCL_CHECK(err, err = m_q.enqueueTask(m_kernel));

    // Wait for OpenCL operations to finish (ie kernel data xfer to PLRAM)
    m_q.finish();
#if 0
    //Copy result from Device Global Memory back to Host Local Memory
    OCL_CHECK(err, err = m_q.enqueueMigrateMemObjects(
                {m_sharedBufGM},CL_MIGRATE_MEM_OBJECT_HOST));

    // Wait for OpenCL operations to finish
    m_q.finish();
#endif
    // has the contents of one of the HBMs been requested?
    int hbm_idx = -1;
    if(m_shmem)
    {
        int idx = m_shmem->check_for_request();
        if( (idx >= 0)
            && (idx < m_num_hbm)
            && m_is_hbm_shared.at(idx) )
        {
            std::cout << " -> xfer HBM[" << idx <<"] from FPGA ";
            hbm_idx = idx;
            // Mark host buffer
            size_t bytes_watermarked = m_hbm_size_bytes.at(idx);
            if (bytes_watermarked > (1<<20) )
                bytes_watermarked = (1<<20);
            memset(m_hostBufs.at(idx).data(), 0x55, bytes_watermarked);
            
            auto time_start = std::chrono::system_clock::now();
            OCL_CHECK(err, err = m_q.enqueueMigrateMemObjects(
                //{m_sharedBufGM,m_fpgaBufs.at(idx)},CL_MIGRATE_MEM_OBJECT_HOST));
                {m_fpgaBufs.at(idx)},CL_MIGRATE_MEM_OBJECT_HOST));

            // Wait for OpenCL operations to finish
            m_q.finish();
            auto time_end = std::chrono::system_clock::now();

            std::chrono::duration<double> secs_xfer = time_end - time_start;
            float bytes_per_sec = (float) (m_hbm_size_bytes.at(idx))
                                        / secs_xfer.count();
            std::cout << " ["
                << (float)(m_hbm_size_bytes.at(idx)) << "bytes, "
                << bytes_per_sec << " bytes/sec]" << std::endl;
        }
        else {
            //Copy result from Device Global Memory back to Host Local Memory
            OCL_CHECK(err, err = m_q.enqueueMigrateMemObjects(
                {m_sharedBufGM},CL_MIGRATE_MEM_OBJECT_HOST));

            // Wait for OpenCL operations to finish
            m_q.finish();
        }
    }
    else
    {
    //Copy result from Device Global Memory back to Host Local Memory
    OCL_CHECK(err, err = m_q.enqueueMigrateMemObjects(
                {m_sharedBufGM},CL_MIGRATE_MEM_OBJECT_HOST));

    // Wait for OpenCL operations to finish
    m_q.finish();
    }

    // Copy results from shared buffer to buffer provided by caller
    for(unsigned int i=0; i<len; i++)
        buf[i] = m_sharedBuf[i];

    if(hbm_idx >= 0)
    {
        std::cout << " -> Copying HBM data [" << std::hex
                << "0x" << m_hostBufs.at(hbm_idx)[0]
                << ", 0x" << m_hostBufs.at(hbm_idx)[1]
                << " ...] to SHM for file writer"
                << std::dec << std::endl;
        size_t len = m_hbm_size_bytes.at(hbm_idx);
        if( len > (1<<30)) // TODO fixme: posix shared mem is hard-coded 1Gbyte
            len = (1<<30);
        //m_hostBufs[hbm_idx][0] = 0x0df0feca; // temp marker to show transfer happened
        m_shmem->wait_write_to_share(m_hostBufs.at(hbm_idx).data()
                , m_hbm_size_bytes.at(hbm_idx));
        //std::cout << " -> HBM data is in Posix SHM " << hbm_idx << std::endl;
    }
}

void Alveo_cl::write(uint32_t addr, uint32_t * buf, uint32_t len)
{
    // ARGS-space byte addresses of 32-bit register words to be transferred
    int argsSrcAddr = m_shared_base_addr * 4; // Addr of RAM shared with VITIS
    int argsDestAddr = addr * 4;  // Address of destination
    int dmaLength = len * 4;  // Bytes to copy (one 32-bit word = 4 bytes)

    cl_int err;
    // Set OpenCL kernel arguments sent to FPGA
    OCL_CHECK(err, err = m_kernel.setArg(0, argsSrcAddr));
    OCL_CHECK(err, err = m_kernel.setArg(1, argsDestAddr));
    OCL_CHECK(err, err = m_kernel.setArg(2, m_sharedBufGM));
    OCL_CHECK(err, err = m_kernel.setArg(3, dmaLength));
    //OCL_CHECK(err, err = m_kernel.setArg(4, m_intl0BufGM));
    //OCL_CHECK(err, err = m_kernel.setArg(5, m_intl1BufGM));
    for(int i=0; i<m_num_hbm; i++)
    {
        OCL_CHECK(err, err = m_kernel.setArg(4+i, m_fpgaBufs.at(i)));
    }

    //Copy input data from caller buffer to device global memory
    for(unsigned int i=0; i<len; i++)
        m_sharedBuf[i] = buf[i];
    OCL_CHECK(err, err = m_q.enqueueMigrateMemObjects(
                {m_sharedBufGM, },0/* 0 means from host*/));

    //Launch the OpenCL kernel
    OCL_CHECK(err, err = m_q.enqueueTask(m_kernel));
    // Wait for OpenCL operations to finish
    m_q.finish(); // TODO? Wait could be done at start of read/write instead
}
