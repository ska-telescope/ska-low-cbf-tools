/* A class to interpret gemini-protocol packets received by a Gemini server
 * performing the register-read or register-write operations requested
 */
/* Copyright (c) 2020, CSIRO. */

#include "gemini.h"
#include <iostream>
#include <string>
#include <cstring>

Gemini::Gemini(std::function<void(uint32_t, int, char*)> writer
              ,std::function<void(uint32_t, int, char*)> reader)
    : m_writer(writer)
    , m_reader(reader)
{
}

Gemini::~Gemini()
{
}

// Process a Gemini packet in 'buf', length 'rx_len_bytes'.
// Result data will overwrite data in 'buf'. Buffer size is 'tx_len_bytes'.
void Gemini::pkt_in(char * buf, size_t rx_len_bytes, size_t * tx_len_bytes )
{
    int max_tx_len_bytes = *tx_len_bytes;
    *tx_len_bytes = 0; // Default: returning zero bytes

    if(rx_len_bytes < 12)
    {
        std::cout << "Packet too short for Gemini Protocol" << std::endl;
        return;
    }

    if(buf[0] != 1)
    {
        std::cout << "Expecting Gemini Protocol v1, got v"
            << (uint16_t) buf[0] << std::endl;
        return;
    }

    // Note: this shortcut assumes we're on a little-endian computer
    uint32_t * word = (uint32_t *) buf;

    switch( buf[1] ) // buf[1] contains the command in the Gemini Packet
    {
        case 0x1: // "Connect" command
            //std::cout << "connect " << std::endl;
            // Always reply to "Connect" with "ACK"
            buf[1] = 0x10;  // ACK
            buf[3] = buf[2];// server sequence no <- client sequence no
            word[2] = 0x03; // 3 register payload
            word[3] = 350;  // Max supported registers in payload
            word[4] = 1;    // pipeline depth = 1 (wait for each transaction)
            word[5] = 13;   // connection id
            m_last_seq = buf[2];
            *tx_len_bytes = 24;
            break;
        case 0x03: // read_inc command
            //std::cout << "  read" << std::endl;
            if(buf[2] == m_last_seq)
                return;
            {
                int read_len_bytes = word[2] & 0x00ffff;
                uint32_t addr = word[1];
                if( max_tx_len_bytes < (4*read_len_bytes + 12))
                {
                    std::cout << "requested more bytes than buffer space"
                        << std::endl;
                    return;
                }

                // Perform the register read
                if(m_reader)
                {
                    m_reader(addr, read_len_bytes, (char *)(word+3));
                }
                else
                {
                    // Just return zeros if we have no reader-function
                    for(int i=0; i<read_len_bytes; i++)
                        word[3+i] = 0x0;
                }

                // Format reply packet that will be sent to client
                buf[1] = 0x10;  // ACK
                buf[3] = buf[2];// server sequence no <- client sequence no
                m_last_seq = buf[2];
                word[2] = read_len_bytes;
                *tx_len_bytes = read_len_bytes * 4 + 12;
            }
            break;
        case 0x05: // write inc command
            //std::cout << "  write" << std::endl;
            if(buf[2] == m_last_seq)
                return;
            {
                int write_len = word[2] & 0x00ffff;
                uint32_t addr = word[1];

                // Perform the register w rite
                if(m_writer)
                {
                    m_writer(addr, write_len, (char *)(word+3));
                }

                // Format reply packet that will be sent to client
                buf[1] = 0x10;  // ACK
                buf[3] = buf[2];// server sequence no <- client sequence no
                word[2] = 0;
                *tx_len_bytes = 12;
            }
            break;
        default:
            std::cout << "*  unknown gemini cmd" << std::endl;
            break;
    }
    return;
}
