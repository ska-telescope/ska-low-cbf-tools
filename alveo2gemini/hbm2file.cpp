/* A program to request HBM data from alveo2gemin program via Posix shared mem,
 *  and write it to a disk file.
 */
/* Copyright (c) 2020, CSIRO. */

#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h> // for getopt
//#include <errno.h>

#include "hbm_dump_sharedmem.h"


void usage(const char *progname)
{
    std::cout << "USAGE:" << std::endl;
    std::cout << progname <<  " [-d alveo_idx] [-i hbm_idx]"
        << " [-f output_filename]" << std::endl;
    return;
}


int main(int argc, char * argv[])
{
    std::cout << "Start.." << std::endl;
    // Default parameters
    uint32_t hbm_idx = 0;
    int device_idx = 0;
    std::string file_name("hbm.dat");

    // Get command line options if any
    int ret;
    while((ret = getopt(argc, argv, "d:f:i:v")) != -1)
    {
        switch(ret)
        {
            case 'd':
                device_idx = std::stol(optarg);
                break;
            case 'f':
                file_name = std::string(optarg);
                break;
            case 'i':
                hbm_idx = std::stoul(optarg);
                break;
            case 'v':
                std::cout << "Last modified: " << LAST_MODIFIED_DATE
                        << "\nGit version  : " << GIT_VERSION << std::endl;
                return 0;
                break;
            default:
                usage(argv[0]);
                return -1;
                break;
        }
    }
    std::cout << "Attaching Posix shared mem" << std::endl;
    bool is_owner = false; // only a client of shared mem, don't own it
    HBMDumpSharedMem * shmem = HBMDumpSharedMem::create(is_owner, device_idx);
    if(shmem == nullptr)
    {
        std::cout << "Could not attach to Posix shared mem" << std::endl;
        return -1;
    }

    std::cout << "Requesting HBM data" << std::endl;
    shmem->dump_request(hbm_idx);

    #define HBM_BYTES (1<<30)
    char *buf = (char *) malloc((size_t) HBM_BYTES);
    std::cout << "Getting data from shmem" << std::endl;
    shmem->wait_read_from_share( buf, (size_t) HBM_BYTES);

    std::cout << "Writing data to file '" << file_name << "'" << std::endl;
    std::ofstream out_file(file_name, std::ios::out | std::ios::binary
            |std::ios::trunc);
    out_file.write(buf, size_t HBM_BYTES);
    out_file.close();

    std::cout << "End." << std::endl;
    free(buf);
    delete shmem;
    return 0;
}

