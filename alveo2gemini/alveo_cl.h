/* A class encapsulating the Xilinx OpenCL interface for Alveo cards.
 * Based on example code provided by Xilinx.
 */

/**********
Copyright (c) 2018, Xilinx, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********/

/* Copyright (c) 2020, CSIRO */


#ifndef ALVEO_CL_H
#define ALVEO_CL_H

#define CL_HPP_CL_1_2_DEFAULT_BUILD
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY 1
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <vector>
#include <string>
#include <CL/cl2.hpp>
#include "aligned_allocator.h"

class HBMDumpSharedMem;

typedef std::vector<int, aligned_allocator<int>> Host_buffer;

class Alveo_cl
{
    private:
        int m_device_idx;
        Host_buffer m_sharedBuf;
        cl::Buffer m_sharedBufGM;
        std::vector<Host_buffer> m_hostBufs;
        std::vector<cl::Buffer> m_fpgaBufs;
        uint32_t m_shared_base_addr;
        int m_num_hbm;
        std::vector<uint64_t> m_hbm_size_bytes;
        std::vector<bool> m_is_hbm_shared;

        // ordering of the next four is needed for proper openCL cleanup
        cl::Context m_context;
        cl::CommandQueue m_q;
        cl::Program m_program;
        cl::Kernel m_kernel;

        HBMDumpSharedMem * m_shmem;

        void hbm_configuration(std::string hbm_config);
    public:
        Alveo_cl(int device_idx);
        ~Alveo_cl();
        int init(std::string  xclbin_filename, std::string kernel_name
                , uint32_t args_base, std::string hbm_config, bool wait = false);
        void write(uint32_t addr, uint32_t * buf, uint32_t len);
        void read (uint32_t addr, uint32_t * buf, uint32_t len);

};

#endif
