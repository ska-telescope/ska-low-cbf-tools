# Program to download contents of 4Gbyte Correlator Coarse Corner Turner RAM
#   
#
# Copyright CSIRO 2020

import sys
import time
import subprocess
from random import randrange
from threading import Thread
from time import sleep
from I_Gemini_Client import Client, ClientError, ServerErrorDecode, Peripheral, Slave, RegisterBlock, EventObserver, ARRAYCODE
from array import array

NUM_PAGES = 1024 # 4 Gbyte of RAM is accessible

def hbm_download(fpga, filename):
    periph = Peripheral('dsp_top')

    ram = Slave(periph, 'hbmdebug').ram('data')
    ram_data = array(ARRAYCODE, [0 for i in range(0,ram.len)])

    page_reg = Slave(periph, 'statctrl').regs('hbmpage')
    page_reg_val = array(ARRAYCODE, [0 for i in range(0,page_reg.len)])

    with open(filename, 'wb') as f:
        t_start = time.time()
        for page in range(0,NUM_PAGES):
            page_reg_val[0] = page << 22
            fpga.write(page_reg, page_reg_val)
    
            fpga.read(ram, ram_data)
            f.write(ram_data)
        t_stop = time.time()
    
        num_bytes = ram.len * 4 * NUM_PAGES 
        elapsed = t_stop - t_start

    print("*** Correlator CTC HBM at {} downloaded to file: {} ***".format(fpga_ip, filename))
    print('{} Mbytes, {:.2f} seconds, {:.2f} Mbps'.format(num_bytes/(1024*1024),
                elapsed, ((num_bytes * 8 /elapsed) * 1e-6)))

    return


if __name__ == '__main__':

  fpga_ip = '10.32.0.4' # Default address of Gemini HBM to download from 
  filename = "hbm_dat.bin" # Default name of output file

  # override default args from command line if option args provided
  if len(sys.argv) >=2:
    fpga_ip = sys.argv[1]  # first optional arg is IP addr of Gemini Card
    if len(sys.argv) >= 3:
      filename = sys.argv[2] # 2nd optional arg is name of output file

  # Connect to Gemini Card
  fpga = Client()
  fpga.trace = False
  fpga.timeoutUSecs = 10000
  fpga.clientMtu = 8000
  fpga.connect(fpga_ip)
  # Check that we really have connected to Gemini Card
  for i in range(2):
      if fpga.connected:
          break
      sleep(1)
  if not fpga.connected:
      print("Failed to connect to FPGA at {}".format(fpga_ip))
      sys.exit(1)
  
  hbm_download(fpga, filename)

  print('')
  sys.exit(0)

