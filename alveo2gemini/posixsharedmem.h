#ifndef POSIXSHAREDMEM
#define POSIXSHAREDMEM

/* A shared-memory base class using Posix shared memory interface which is
 * newer than system-v shared memory. Intended for very fast inter-process
 * communication, since memory copies are not mediated by the Linux kernel.
 *
 * The constructor requires a name which must start with "/", a memory size,
 * and a flag indicating whether we want to work with already existing shared
 * memory (as a client) or if we want to be the creator of the shared memory
 * instead.
 *
 * The result of construction is the address contained in m_addr, the base
 * address of the shared memory. If construction fails m_addr will be nullptr.
 */

#include <string>

class PosixSharedMem
{
    private:
        std::string m_name; // name of shared mem (see in /dev/shm)
        size_t m_size;      // size in bytes of shared mem
        bool m_owned;       // flag if this process owns (and frees) the mem
        int m_fd;
    protected:
        void * m_addr;      // The mapped-in-process address of the share
    public:
        PosixSharedMem(std::string name, size_t size, bool owner=false);
        
        // No copying
        PosixSharedMem(const PosixSharedMem &) = delete;
        PosixSharedMem & operator=(const PosixSharedMem &) = delete;

        virtual ~PosixSharedMem();
};

#endif
