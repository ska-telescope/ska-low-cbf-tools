# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Test the ConfigurableCommand class"""
# pylint doesn't like pytest conventions
# pylint: disable=redefined-outer-name,no-self-use

import shutil

import pytest

from tango_cmdline.configurable_command import ConfigurableCommand


@pytest.fixture
def config_cmd():
    """A sample ConfigurableCommand"""
    return ConfigurableCommand("ls -la $path")


@pytest.mark.usefixtures("config_cmd")
class TestConfigurableCommand:
    """Test the ConfigurableCommand class"""

    def test_init(self, config_cmd):
        """exercise the fixture, ensuring the class can be instantiated"""

    def test_args(self, config_cmd):
        """Check that variable is parsed from template string"""
        assert "path" in config_cmd.args

    def test_use_arg(self, config_cmd):
        """Set then get an argument that exercises the shell escaper"""
        trick_input = "$(trick)"  # result will be "'$(trick)'"
        config_cmd["path"] = trick_input
        assert trick_input in config_cmd["path"]  # it should be in there
        assert trick_input != config_cmd["path"]  # but with some wrapping

    def test_execute(self, config_cmd):
        """Execute a command"""
        config_cmd["path"] = "."
        config_cmd.execute()

    def test_pids(self):
        """Run multiple slow commands and check their PIDs are collected"""
        sleeper = ConfigurableCommand("sleep $duration")
        sleeper["duration"] = 30  # also tests non-str argument
        n_instances = 5
        for _ in range(n_instances):
            sleeper.execute()

        assert len(sleeper.pids) == n_instances

    def test_stdout(self):
        echo = ConfigurableCommand(shutil.which("echo") + " -n $message")
        test_message = "Hello $World * ! ; [ ( ) ]"
        echo["message"] = test_message
        echo.execute()
        while echo.pids:  # wait for command to complete
            pass
        assert echo.stdout == test_message

    def test_stderr(self):
        cat = ConfigurableCommand(shutil.which("cat") + " $path")
        test_path = "this_file_does_not_exist_2_71828"
        cat["path"] = test_path
        cat.execute()
        while cat.pids:  # wait for command to complete
            pass
        assert test_path in cat.stderr
