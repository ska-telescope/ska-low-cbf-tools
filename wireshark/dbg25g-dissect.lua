--[[
    Wireshark dissector for 25G debug packet output. See 'Debug output format' in
    https://confluence.skatelescope.org/display/SE/Internal+Timing+and+Communications

    CSIRO 23 Jan 2020
--]]

-- define debug levels
local debug_level = {
    DISABLED = 0,
    LEVEL_1  = 1,
    LEVEL_2  = 2
}

-- set this DEBUG to debug_level.LEVEL_1 to enable printing debug_level info
-- set it to debug_level.LEVEL_2 to enable really verbose printing
-- note: this will be overridden by user's preference settings
local DEBUG = debug_level.LEVEL_1

local default_settings =
{
    debug_level  = DEBUG,
}

-- for testing purposes, we want to be able to pass in changes to the defaults
-- from the command line; because you can't set lua preferences from the command
-- line using the '-o' switch (the preferences don't exist until this script is
-- loaded, so the command line thinks they're invalid preferences being set)
-- so we pass them in as command arguments insetad, and handle it here:
local args={...} -- get passed-in args
if args and #args > 0 then
    for _, arg in ipairs(args) do
        local name, value = arg:match("(.+)=(.+)")
        if name and value then
            if tonumber(value) then
                value = tonumber(value)
            elseif value == "true" or value == "TRUE" then
                value = true
            elseif value == "false" or value == "FALSE" then
                value = false
            elseif value == "DISABLED" then
                value = debug_level.DISABLED
            elseif value == "LEVEL_1" then
                value = debug_level.LEVEL_1
            elseif value == "LEVEL_2" then
                value = debug_level.LEVEL_2
            else
                error("invalid commandline argument value")
            end
        else
            error("invalid commandline argument syntax")
        end

        default_settings[name] = value
    end
end

local dprint = function() end
local dprint2 = function() end
local function reset_debug_level()
    if default_settings.debug_level > debug_level.DISABLED then
        dprint = function(...)
            print(table.concat({"Lua:", ...}," "))
        end

        if default_settings.debug_level > debug_level.LEVEL_1 then
            dprint2 = dprint
        end
    end
end
-- call it now
reset_debug_level()

dprint2("Wireshark version = ", get_version())
dprint2("Lua version = ", _VERSION)

----------------------------------------
-- check for older buggy versions
local major, minor, micro = get_version():match("(%d+)%.(%d+)%.(%d+)")
if major and tonumber(major) <= 1 and ((tonumber(minor) <= 10) or (tonumber(minor) == 11 and tonumber(micro) < 3)) then
        error(  "Wireshark ("..get_version()..") is old!\n"..
                "Requires Wireshark version 1.11.3 or higher.\n" )
end
assert(ProtoExpert.new, "ProtoExpert missing - get Wireshark 1.11.3 or higher")

----------------------------------------


----------------------------------------
-- create Proto object that will be registered after configuration
local dbg0 = Proto("dbg25g","Perentie 25G Debug")

-- provide less verbose access to the fields part of the protocol object
local f = dbg0.fields

-- Description of each of the possible fields in the Gemini Protocol
f.pf_version = ProtoField.uint8("dbg0ini.ver", "Version")
--f.pf_version = ProtoField.uint8("dbg0ini.ver", "Version", base.HEX, nil, 0xFF)
--local dbg0_cmds = {[1] = "Connect", [2] = "Read_FIFO", [3] = "Read_INCR", [4] = "Write_FIFO", [5] ="Write_INCR", [0x10]="ACK", [0x20]="NAK_temp", [0x40]="NAK_perm", [0x80]="Publish"}
local dbg0_pkt_types = {[1] = "LFAA Ingest", [2] = "Correlator Filterbank output"}
local ic_in_types = {[0]="Z_in[0]", [1]="Z_in[1]", [2]="Z_in[0]", [3]="Z_in[1]", [4]="Z_in[4]", [5]="Z_in[5]", [6]="Z_in[6]",
                     [7]="X_in[0]", [8]="X_in[1]", [9]="X_in[2]", [10]="X_in[3]", [11]="X_in[4]",
                     [12]="Y_in[0]", [13]="Y_in[1]", [14]="Y_in[0]", [15]="Y_in[1]",[16]="Y_in[4]", 
                     [17]="Timing out", [18]="LFAA_a_in", [19]="LFAA_b_in", [20]="Correlator_FB"}
local ic_out_types = {[0]="Z_out[0]", [1]="Z_out[1]", [2]="Z_out[0]", [3]="Z_out[1]", [4]="Z_out[4]", [5]="Z_out[5]", [6]="Z_out[6]",
                      [7]="X_out[0]", [8]="X_out[1]", [9]="X_out[2]", [10]="X_out[3]", [11]="X_out[4]",
                      [12]="Y_out[0]", [13]="Y_out[1]", [14]="Y_out[0]", [15]="Y_out[1]",[16]="Y_out[4]", 
                      [17]="Timing out", [18]="CoarseCorner_out", [19]="Correlator"}

-- Routing header decodings
f.pf_seq =   ProtoField.uint16("dbg0.seq",      "Debug Seq",    base.HEX, nil, nil)
f.pf_icin  = ProtoField.uint8 ("dbg0.ic_in",    "In Port  ",    nil, ic_in_types,  nil)
f.pf_icout = ProtoField.uint8 ("dbg0.ic_out",   "Out Port ",    nil, ic_out_types, nil)
f.pf_xyz =   ProtoField.uint16("dbg0.my_xyz",   "Local XYZ",    base.HEX, nil, nil)
f.pf_src1 =  ProtoField.uint16("dbg0.src1",     "Src_1 XYZ",    base.HEX, nil, nil)
f.pf_src2 =  ProtoField.uint16("dbg0.src2",     "Src_2 XYZ",    base.HEX, nil, nil)
f.pf_src3 =  ProtoField.uint16("dbg0.src3",     "Src_3 XYZ",    base.HEX, nil, nil)
f.pf_dest =  ProtoField.uint16("dbg0.dest",     "Dest XYZ ",    base.HEX, nil, nil)
f.pf_type =  ProtoField.uint8 ("dbg0.pkt_type", "Pkt Type ",    nil, dbg0_pkt_types, nil)

-- Type 1 packet header decodings (ingest packets)
f.pf_spead =  ProtoField.uint32("dbg0.spead_cnt", "Spead count", base.HEX, nil, nil)
f.pf_vchan =  ProtoField.uint16("dbg0.virt_chan", "Virt channl", base.HEX, nil, nil)
f.pf_freq =  ProtoField.uint16 ("dbg0.chan_freq", "Chanl Freq ", base.HEX, nil, nil)
f.pf_stnid =  ProtoField.uint16("dbg0.stn_id",    "Station ID ", base.HEX, nil, nil)
f.pf_stnsl =  ProtoField.uint8 ("dbg0.stn_sel",   "Station Sel", base.HEX, nil, nil)

-- Type 2 packet header decodings (correlator filterbank packets)
f.pf_time =  ProtoField.uint32("dbg0.timestamp",  "Timestamp",   base.HEX, nil, nil)
f.pf_sid1 =  ProtoField.uint32("dbg0.stn_id1",    "Station ID1", base.HEX, nil, nil)
f.pf_sid2 =  ProtoField.uint32("dbg0.stn_id2",    "Station ID2", base.HEX, nil, nil)
f.pf_fine =  ProtoField.uint32("dbg0.fine_ch",    "Fine channl", base.HEX, nil, nil)
f.pf_cse1 =  ProtoField.uint32("dbg0.coarse1",    "CoarseDel 1", base.HEX, nil, nil)
f.pf_cse2 =  ProtoField.uint32("dbg0.coarse2",    "CoarseDel 2", base.HEX, nil, nil)



----------------------------------------
-- Expert info fields
local ef_query     = ProtoExpert.new("dbg0ini.query.expert", "DNS query message",
                                     expert.group.REQUEST_CODE, expert.severity.CHAT)
local ef_response  = ProtoExpert.new("dbg0ini.response.expert", "DNS response message",
                                     expert.group.RESPONSE_CODE, expert.severity.CHAT)
local ef_ultimate  = ProtoExpert.new("dbg0ini.response.ultimate.expert", "DNS answer to life, the universe, and everything",
                                     expert.group.COMMENTS_GROUP, expert.severity.NOTE)
-- some error expert info's
local ef_too_short = ProtoExpert.new("dbg0ini.too_short.expert", "25G debug packet too short",
                                     expert.group.MALFORMED, expert.severity.ERROR)
local ef_bad_query = ProtoExpert.new("dbg0ini.query.missing.expert", "DNS query missing or malformed",
                                     expert.group.MALFORMED, expert.severity.WARN)
-- add expert info into the proto object
dbg0.experts = { ef_query, ef_too_short, ef_bad_query, ef_response, ef_ultimate }



----------------------------------------
--[[ This function is the callback that Wireshark will call when it finds 
     UDP packets to/from the Old 25G Debug ports that we register. 
     The callback generates information about the 25G Debug part
     of the UDP packet that will be displayed by Wireshark
        'tvbuf' is a Tvb buffer object containing packet bytes,
        'pktinfo' is a Pinfo object, and 
        'root' is a TreeItem object to which we append our dissection text.
--]]
--dbg0.dissector = function (tvbuf,pkt,root)
function dbg0.dissector(tvbuf,pktinfo,root)
    dprint2("dbg0.dissector called")

    -- set the protocol column to show our protocol name
    --pktinfo.cols.protocol:set("Gemini_v" .. tvbuf:range(0,1))
    pktinfo.cols.protocol:set("25G debug")

    local pktlen = tvbuf:reported_length_remaining()

    --[[ Add Gemini protocol to the dissection display tree and get tree
         object so we can add subtree items under it
    --]]
    local tree = root:add(dbg0, tvbuf:range(0,pktlen))

    -- Guard against random packets arriving at debug UDP ports
    local DBG0_MIN_LEN = 14
    if pktlen < DBG0_MIN_LEN then
        tree:add_proto_expert_info(ef_too_short)
        dprint("packet length",pktlen,"too short")
        return
    end

    -- Add dissected values into subtree 
    local bytes_parsed = 0
    tree:add(f.pf_xyz,   tvbuf:range(0,2):le_uint())
    tree:add(f.pf_icin,  tvbuf:range(2,1):uint())
    tree:add(f.pf_icout, tvbuf:range(3,1):uint())
    tree:add(f.pf_seq,   tvbuf:range(4,2):uint())

    local pkt_type = tvbuf:range(7,1):uint()
    local src1 = tvbuf:range(8,2):le_uint()
    src1 = bit32.band(src1, 0x0fff)
    local src2 = tvbuf:range(9,2):le_uint()
    src2 = bit32.rshift(src2,4)
    local src3 = tvbuf:range(11,2):le_uint()
    src3 = bit32.band(src3, 0x0fff)
    local dest = tvbuf:range(12,2):le_uint()
    dest = bit32.rshift(dest, 4)

    tree:add(f.pf_type,  pkt_type)
    tree:add(f.pf_src1, src1)
    tree:add(f.pf_src2, src2)
    tree:add(f.pf_src3, src3)
    tree:add(f.pf_dest, dest)
    
    bytes_parsed = DBG0_MIN_LEN
    -- Enough bytes to decode the next header?
    if pktlen < DBG0_MIN_LEN + 16 then
        tree:add_proto_expert_info(ef_too_short)
        dprint("packet length",pktlen,"too short for type 1")
        return bytes_parsed
    end

    if (pkt_type == 0x01)
    then
        -- These decodings are for new Spead decode packet format implemented in
        -- file LFAAProcess.vhd. For description of the format see:
        --    https://confluence.skatelescope.org/display/SE/LFAA+SPEAD+Decoder
        pkt_hdr = tvbuf:range(DBG0_MIN_LEN, 16)
        local spead_cnt = pkt_hdr:range(0,4):le_uint()
        local virt_chan = pkt_hdr:range(4,2):le_uint()
        local chan_freq = pkt_hdr:range(6,2):le_uint()
        local station_id = pkt_hdr:range(8,2):le_uint()
        local stn_sel = pkt_hdr:range(10,1):le_uint()
        stn_sel = bit32.band(stn_sel, 0x01)

        tree:add(f.pf_spead, spead_cnt)
        tree:add(f.pf_vchan, virt_chan)
        tree:add(f.pf_freq, chan_freq)
        tree:add(f.pf_stnid, station_id)
        tree:add(f.pf_stnsl, stn_sel)

        bytes_parsed = bytes_parsed + 16
    elseif (pkt_type == 0x02) then
        -- These decodings are for new Correlator filter output format implemented in
        -- file LFAAProcess.vhd. For description of the format see:
        --    https://confluence.skatelescope.org/display/SE/LFAA+SPEAD+Decoder
        pkt_hdr = tvbuf:range(DBG0_MIN_LEN, 16)
        local timestamp = pkt_hdr:range(0,4):le_uint()
        local virt_chan = pkt_hdr:range(4,2):le_uint()
        local station_id0 = pkt_hdr:range(6,2):le_uint()
        local station_id1 = pkt_hdr:range(8,2):le_uint()
        local fine_chan = pkt_hdr:range(10,2):le_uint()
        local coarse_del0 = pkt_hdr:range(12,2):le_uint()
        local coarse_del1 = pkt_hdr:range(14,2):le_uint()

        tree:add(f.pf_time, timestamp)
        tree:add(f.pf_vchan, virt_chan)
        tree:add(f.pf_sid1, station_id0)
        tree:add(f.pf_sid2, station_id1)
        tree:add(f.pf_fine, fine_chan)
        tree:add(f.pf_cse1, coarse_del0)
        tree:add(f.pf_cse2, coarse_del1)

        bytes_parsed = bytes_parsed + 16
    else
        dprint("Unknown header type ", pkt_type)
    end

    -- tell wireshark how much of the buffer was parsed/dissected
    dprint2("dbg0.dissector returning",bytes_parsed)
    return bytes_parsed
end

----------------------------------------

-- Register protocol dissector based on UDP port
--   Old 25G debug uses UDP port 0x5678 = 22136. 
--   New 25G debug uses UDP port 0x5679 = 22137.
DissectorTable.get("udp.port"):add(22137, dbg0)

