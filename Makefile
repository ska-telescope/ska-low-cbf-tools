# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag
PROJECT = ska-low-cbf-tools

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-low-cbf-tools

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
WEBJIVE ?= false# Enable Webjive
MINIKUBE ?= true ## Minikube or not

CI_PROJECT_PATH_SLUG ?= ska-low-cbf-tools
CI_ENVIRONMENT_SLUG ?= ska-low-cbf-tools


#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/release.mk
include .make/make.mk
include .make/python.mk
include .make/oci.mk
include .make/help.mk
include .make/docs.mk

# define private overrides for above variables in here
-include PrivateRules.mak

OCI_IMAGES = ska-low-cbf-lfaasim ska-low-cbf-capture-analyse
OCI_IMAGE_BUILD_CONTEXT = $(PWD)

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src

PYTHON_VARS_AFTER_PYTEST = -m 'not post_deployment' --forked \
						--disable-pytest-warnings

PYTHON_BUILD_TYPE = non_tag_setup

PYTHON_SWITCHES_FOR_FLAKE8=--ignore=F401,W503,E203
PYTHON_SWITCHES_FOR_PYLINT=--fail-under=7

