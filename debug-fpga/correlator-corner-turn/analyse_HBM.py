# Convert downloaded HBM contents into data for each channel and station,
# assuming data was written according to the 4 buffer scheme as documented
# in confluence (confluence.skatelescope.org/display/SE/Correlator+Coarse+Corner+Turner)
# The main buffer is assumed to be 410 x 820 blocks, where each block is 8192 bytes.
# Within the buffer, four overlapping buffers are defined, each 384 x 768.
# 

import numpy as np
import matplotlib.pyplot as plt

dataDir = 'run5/'
# HBMPhase is 0 to 11; Mod 4 for the 4 different phases for the way data is written to the main HBM;
# mod 3 to select the auxiliary buffer.
# After reset, the first phase written by the corner turn is HBMPhase = 1
HBMPhase = 1       

checkData_filename = dataDir + 'HBMStream_fpga1.raw'
checkPtrs_filename = dataDir + 'HBMStream_fpga1.txt'
# data after running with the LFAA simulator
#HBMData_filename = dataDir + 'hbm_data.bin'
HBMData_filename = dataDir + 'after_packets.bin'

#HBMData_filename = dataDir + 'hbm_data_auxmain.bin'   # first data is in the aux buffer in this dataset.
# data after fill with 23.
#HBMData_filename = dataDir + 'hbm_data_0x23.bin'

aux0Addr = 410*820*8192
aux1Addr = 410*820*8192 +   24*768*8192
aux2Addr = 410*820*8192 + 2*24*768*8192


# Get 3 Gbytes from the downloaded HBM data.
HBMData = np.fromfile(HBMData_filename, np.int8, count = 3221225472)

# Check the number of zeros in the main buffer
nonZero = np.count_nonzero(HBMData[0:(8192 * 410 * 820)])
zeros = 8192 * 410 * 820 - nonZero
print('In main buffer, {} zeros, {} non zeros.'.format(zeros,nonZero))

# plot 8192 byte blocks with non-default data (default data assumed to be 23)
mainBlockImage = np.zeros((820,410,3))
aux0BlockImage = np.zeros((768,24,3))
aux1BlockImage = np.zeros((768,24,3))
aux2BlockImage = np.zeros((768,24,3))

for block_x in range(410):
    for block_y in range(820):
        blockAddr = block_x * 8192 + block_y * 410 * 8192
        if (np.count_nonzero(HBMData[blockAddr:(blockAddr + 8192)] - 35) > 0):
            if (np.count_nonzero(HBMData[blockAddr:(blockAddr + 8192)]) > 0):
                mainBlockImage[block_y,block_x,0] = 1.0  # comes out red; mix of values (not all values were 0)
            else:
                mainBlockImage[block_y,block_x,1] = 1.0  # comes out green; all values were zero.
        else:
            mainBlockImage[block_y,block_x,2] = 1.0  # comes out blue. All values were 23

for block_x in range(24):
    for block_y in range(768):
        blockAddr = aux0Addr + block_x * 8192 + block_y * 24 * 8192
        if (np.count_nonzero(HBMData[blockAddr:(blockAddr + 8192)] - 35) > 0):
            if (np.count_nonzero(HBMData[blockAddr:(blockAddr + 8192)]) > 0):
                aux0BlockImage[block_y,block_x,0] = 1.0  # red; mix of values (not all values were 0)
            else:
                aux0BlockImage[block_y,block_x,1] = 1.0  # green; all values were zero.
        else:
            aux0BlockImage[block_y,block_x,2] = 1.0  # blue. All values were 23
            
        blockAddr = aux1Addr + block_x * 8192 + block_y * 24 * 8192
        if (np.count_nonzero(HBMData[blockAddr:(blockAddr + 8192)] - 35) > 0):
            if (np.count_nonzero(HBMData[blockAddr:(blockAddr + 8192)]) > 0):
                aux1BlockImage[block_y,block_x,0] = 1.0  # red; mix of values (not all values were 0)
            else:
                aux1BlockImage[block_y,block_x,1] = 1.0  # green; all values were zero.
        else:
            aux1BlockImage[block_y,block_x,2] = 1.0  # blue. All values were 23

        blockAddr = aux2Addr + block_x * 8192 + block_y * 24 * 8192
        if (np.count_nonzero(HBMData[blockAddr:(blockAddr + 8192)] - 35) > 0):
            if (np.count_nonzero(HBMData[blockAddr:(blockAddr + 8192)]) > 0):
                aux2BlockImage[block_y,block_x,0] = 1.0  # red; mix of values (not all values were 0)
            else:
                aux2BlockImage[block_y,block_x,1] = 1.0  # green; all values were zero.
        else:
            aux2BlockImage[block_y,block_x,2] = 1.0  # blue. All values were 23
        
        
plt.figure(1)
plt.imshow(mainBlockImage)
plt.title('Main : green 0, blue 35, red mixed')

plt.figure(2)
plt.imshow(aux0BlockImage)
plt.title('Aux0 : green 0, blue 35, red mixed')

plt.figure(3)
plt.imshow(aux1BlockImage)
plt.title('Aux1 : green 0, blue 35, red mixed')

plt.figure(4)
plt.imshow(aux2BlockImage)
plt.title('Aux2 : green 0, blue 35, red mixed')

###########################################################################
# Rearrange the HBM data into streams for each station and virtual channel
# There are 768 station/channel combinations, and 408*2048 time samples.
# four arrays, for vertical and horizontal polarisation, real + imaginary.
HBMData_SC_T_VRe = np.zeros((768,408*2048),dtype = np.int8)
HBMData_SC_T_VIm = np.zeros((768,408*2048),dtype = np.int8)
HBMData_SC_T_HRe = np.zeros((768,408*2048),dtype = np.int8)
HBMData_SC_T_HIm = np.zeros((768,408*2048),dtype = np.int8)

for sc in range(768):
    # Copy in data from the main buffer, consisting of the first 384 blocks of 2048 time samples.
    for t in range(384):
        # Each block is 8192 bytes = 2048 time samples x 4 (VRe, VIm, HRe, HIm)
        if ((HBMPhase % 4) == 0):
            block_x = 26 + sc//2
            if ((sc % 2) == 0):
                block_y = 2*t
            else:
                block_y = 2*t + 1            
        elif ((HBMPhase % 4) == 1):
            block_x = t
            block_y = 767 - sc
        elif ((HBMPhase % 4) == 2):
            block_x = 383 - sc//2
            if ((sc % 2) == 0):
                block_y = 819 - 2*t
            else:
                block_y = 819 - 2*t - 1
        else: # HBMPhase == 3
            block_x = 409 - t
            block_y = 52 + sc
        
        tFull = t+24
        
        blockAddr = block_x * 8192 + block_y * 410 * 8192
        HBMData_SC_T_VRe[sc,tFull*2048:(tFull*2048 + 2048)] = HBMData[  blockAddr  :(blockAddr+8192):4]
        HBMData_SC_T_VIm[sc,tFull*2048:(tFull*2048 + 2048)] = HBMData[(blockAddr+1):(blockAddr+8192):4]
        HBMData_SC_T_HRe[sc,tFull*2048:(tFull*2048 + 2048)] = HBMData[(blockAddr+2):(blockAddr+8192):4]
        HBMData_SC_T_HIm[sc,tFull*2048:(tFull*2048 + 2048)] = HBMData[(blockAddr+3):(blockAddr+8192):4]
        
    # Copy in data from the auxiliary buffer, consisting of the last 24 blocks of 2048 time samples.
    for t in range(24):
        if ((HBMPhase % 3) == 1):
            auxBase = aux0Addr
        elif ((HBMPhase % 3) == 2):
            auxBase = aux1Addr
        else: # ((HBMPhase % 3) == 0):
            auxBase = aux2Addr
        
        #tFull = t + 384    # actual time runs from block 384 to 384+24 = 408    
        tFull = t
        
        blockAddr = auxBase + sc*24*8192 + t * 8192
        HBMData_SC_T_VRe[sc,tFull*2048:(tFull*2048 + 2048)] = HBMData[  blockAddr  :(blockAddr+8192):4]
        HBMData_SC_T_VIm[sc,tFull*2048:(tFull*2048 + 2048)] = HBMData[(blockAddr+1):(blockAddr+8192):4]
        HBMData_SC_T_HRe[sc,tFull*2048:(tFull*2048 + 2048)] = HBMData[(blockAddr+2):(blockAddr+8192):4]
        HBMData_SC_T_HIm[sc,tFull*2048:(tFull*2048 + 2048)] = HBMData[(blockAddr+3):(blockAddr+8192):4]
        
#####################################################################
# Get the equivalent data from the LFAA ingest data files
#
checkPtrs = np.loadtxt(checkPtrs_filename, dtype = np.int64, delimiter = ',')
checkData = np.fromfile(checkData_filename, np.int8)

# Check against the expected values.
defaultsChecked = 0
defaultsBad = 0
zerosChecked = 0
zerosBad = 0
dataChecked = 0
dataBad = 0

# Print a bit of the check data and HBM data
print('check data 0:')
print(checkData[0:64])

print('HBM data 7 Vre')
print(HBMData_SC_T_VRe[7,0:64])
print('HBM data Vre')
print(HBMData_SC_T_VIm[7,0:64])
print('HBM data Hre')
print(HBMData_SC_T_HRe[7,0:64])
print('HBM data HIm')
print(HBMData_SC_T_HIm[7,0:64])


for channel in range(128):
    for station in range(6):
        sc = channel*6 + station
        
        # Correct for offset by one error in the firmware for the station id
        if (station == 0):
            p1 = -1
        else:
            p1 = checkPtrs[channel,station-1]
            
        if (p1 == -1):
            # No data written; check against the default (only valid if HBM was cleared prior to use)
            defaultsChecked += 408*2048*4
            defaultsBad += np.count_nonzero(HBMData_SC_T_VRe[sc,:] - 35)
            defaultsBad += np.count_nonzero(HBMData_SC_T_VIm[sc,:] - 35)
            defaultsBad += np.count_nonzero(HBMData_SC_T_HRe[sc,:] - 35)
            defaultsBad += np.count_nonzero(HBMData_SC_T_HIm[sc,:] - 35)
        elif (p1 == 0):
            # data should be all zeros
            zerosChecked += 408*2048*4
            zerosBad += np.count_nonzero(HBMData_SC_T_VRe[sc,:])
            zerosBad += np.count_nonzero(HBMData_SC_T_VIm[sc,:])
            zerosBad += np.count_nonzero(HBMData_SC_T_HRe[sc,:])
            zerosBad += np.count_nonzero(HBMData_SC_T_HIm[sc,:])
        else:
            # Compare with checkData((p1-1):...)
            compare_VRe = checkData[(p1-1):(p1-1 + 408*2048*4):4]
            compare_VIm = checkData[(p1-1+1):(p1-1 + 408*2048*4):4]
            compare_HRe = checkData[(p1-1+2):(p1-1 + 408*2048*4):4]
            compare_HIm = checkData[(p1-1+3):(p1-1 + 408*2048*4):4]
            dataChecked += 408*2048*4
            dataBad += np.count_nonzero(HBMData_SC_T_VRe[sc,:] - compare_VRe)
            dataBad += np.count_nonzero(HBMData_SC_T_VIm[sc,:] - compare_VIm)
            dataBad += np.count_nonzero(HBMData_SC_T_HRe[sc,:] - compare_HRe)
            dataBad += np.count_nonzero(HBMData_SC_T_HIm[sc,:] - compare_HIm)
            
print('Checked {} default samples (0x23), {} mismatches'.format(defaultsChecked, defaultsBad))
print('Checked {} zero samples, {} mismatches'.format(zerosChecked, zerosBad))
print('Checked {} non-zero samples, {} mismatches'.format(dataChecked, dataBad))


#####################################################################
# Analyse

   
# Find the RMS for each station/channel
sc_rms = np.zeros((128,6))
for station in range(6):
    for vc in range(128):
        sc_rms[vc,station] = np.std(HBMData_SC_T_HIm[vc*6+station,:])

print('station 1, 2, 3, 4, 5, 6')
for vc in range(128):
    print('channel {} : {} '.format(vc, np.array2string(sc_rms[vc,:], formatter={'float_kind':lambda x: "%.2f" % x})))

# plot some of the data
for f1 in range(7,10):
    plt.figure(f1+4)
    plt.clf()
    plt.plot(HBMData_SC_T_VRe[f1,1:800000])

plt.show()

#print(HBMData[0:16])
#print(HBMData[10000000:10000016])
#print(HBMData[1000000000:1000000016])

#print(mainBlockImage[0:10,0:10])
