#include "posixsharedmem.h"

#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
//#include <semaphore.h>
#include <unistd.h>
#include <iostream> // for cin cout cerr
#include <errno.h>
#include <string.h>

PosixSharedMem::PosixSharedMem(std::string name, size_t size, bool owned)
    : m_name(name)
    , m_size(size)
    , m_owned(owned)
    , m_addr(nullptr)
{
    // create shared memory file descriptor
    if(owned)
    {
        shm_unlink(name.c_str()); // delete any previous instances
        m_fd = shm_open(name.c_str(), O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
    }
    else
    {
        m_fd = shm_open(name.c_str(), O_RDWR, 0);
    }
    if(m_fd == -1)
    {
        std::cerr << "Failed shm_open /dev/shm" << name << ": "
            << strerror(errno) << std::endl;

        return;
    }

    // Set shared memory size
    if (ftruncate(m_fd, m_size) != 0)
    {
        std::cerr << "Ftruncate failed\n";
        return;
    }

    // Map shared memory into the process memory space
    void* a = mmap(NULL, m_size, PROT_READ | PROT_WRITE, MAP_SHARED, m_fd, 0);
    m_addr = (a == ((void*) -1)) ? nullptr: a;
}

PosixSharedMem::~PosixSharedMem()
{
    if(m_addr != nullptr)
    {
        int rv = munmap(m_addr, m_size);
        if(rv != 0)
            std::cerr << "Failed to unmap shared mem\n";
        m_addr = nullptr;
    }
    if(m_fd >=0)
        close(m_fd);
    if(m_owned)
    {
        int rv = shm_unlink(m_name.c_str());
        if(rv != 0)
            std::cerr << "Failed to unlink shared mem\n";
    }

}
