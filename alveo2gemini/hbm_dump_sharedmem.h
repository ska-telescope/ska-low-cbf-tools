#ifndef HBM_DUMP_SHAREDMEM
#define HBM_DUMP_SHAREDMEM

/* A shared memory class for passing HBM data from alveo2gemini to a program
 * that will dump the data to disk
 */

#include "posixsharedmem.h"
#include <semaphore.h>
#include <string>

#define NUM_DATA_BYTES (1<<30) // 1Gbyte

class HBMDumpSharedMem: public PosixSharedMem
{
    private:
        bool m_ok; // whether everything initialised ok.
        bool m_isOwner; // does this object own the shared mem or just use?
        struct Shared_info // Definition of what's in the shared mem
        {
            sem_t updated;
            sem_t requested;
            sem_t access; // access to following data elements
            int hbm_idx; // which hbm to dump
            int8_t hbm_data[NUM_DATA_BYTES]; // data from HBM
        };
        Shared_info * m_mem; // Pointer to the shared data in memory

        HBMDumpSharedMem(bool isOwner, std::string mem_name);
        bool isOk(); // Whether construction was successful
    public:
        static HBMDumpSharedMem * create(bool isOwner, int mem_idx);
        HBMDumpSharedMem(const HBMDumpSharedMem &) = delete;
        HBMDumpSharedMem & operator=(const HBMDumpSharedMem &) = delete;
        ~HBMDumpSharedMem();
        void dump_request(uint32_t hbm_ix);
        int check_for_request();
        void wait_write_to_share(void *buffer, size_t len_bytes);
        void wait_read_from_share( void *buf, size_t len);
};

#endif
